﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmMain : Form
    {
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private List<Empleado> empleados;
        private BindingSource bsEmpleados;
        

        public FrmMain()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            bsEmpleados = new BindingSource();
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            empleados = daoEmpleadoImplements.All();

            empleados.ForEach(ep => {
                dsNomina.Tables["Empleado"].Rows.Add(ep.EmpleadoAsArray());
            });

            bsEmpleados.DataSource = dsNomina;
            bsEmpleados.DataMember = dsNomina.Tables["Empleado"].TableName;

            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        //Metodo para controlar la accesibilidad de los botones
        public void SetButtonEnable(bool flag1, bool flag2)
        {
            button1.Enabled = flag1;
            button2.Enabled = flag2;
        }

        
        //Este es el Boton para agregar a un empleado que desplegara un Dialog
        private void button1_Click(object sender, EventArgs e)
        {
            FrmAgregarEmpleado frmAgregarEmpleado = new FrmAgregarEmpleado();
            frmAgregarEmpleado.DSNomina = dsNomina; 
            frmAgregarEmpleado.ShowDialog(this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //int pos = dgvEmpleados.CurrentRow.Index;
            //dgvEmpleados.Rows.RemoveAt(pos);
            

            int index = dgvEmpleados.SelectedRows[0].Index;
            int id = Convert.ToInt32(dgvEmpleados.Rows[index].Cells["Id"].Value.ToString());
            string cedula = dgvEmpleados.Rows[index].Cells["Cedula"].Value.ToString();
            string nombres = dgvEmpleados.Rows[index].Cells["Nombres"].Value.ToString();
            string apellidos = dgvEmpleados.Rows[index].Cells["Apellidos"].Value.ToString();
            string direccion = dgvEmpleados.Rows[index].Cells["Direccion"].Value.ToString();
            string telefono = dgvEmpleados.Rows[index].Cells["Telefono"].Value.ToString();
            DateTime fechadecontratacion = Convert.ToDateTime(dgvEmpleados.Rows[index].Cells["FechaContratacion"].Value.ToString());
            decimal salario = Convert.ToDecimal(dgvEmpleados.Rows[index].Cells["Salario"].Value.ToString());
            //Ahora creo el empleado
            Empleado empleado = new Empleado()
            {
                Id = id,
                Nombres = nombres,
                Apellidos = apellidos,
                Cedula = cedula,
                Telefono = telefono,
                Direccion = direccion,
                Salario = salario,
                FechaContratacion = fechadecontratacion
            };
            
            //Mensaje de confirmacion
            if (MessageBox.Show("¿Está Seguro que desea eliminar los datos referentes a este empleado?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                //Elimina el registro
                dgvEmpleados.Rows.RemoveAt(index);
                daoEmpleadoImplements.Delete(empleado);
            }
            else
            {
                MessageBox.Show("Error, Debe seleccionar una fila antes de eliminar algun registro", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
}

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //El metodo lo sawue de internet porque no fui capaz de hacerlo desde cero yo solo
            DataView data = dsNomina.Tables[0].DefaultView;
            data.RowFilter = "Nombres LIKE '%" + txtBuscar.Text + "%'" + "OR Apellidos LIKE '%" + txtBuscar.Text + "%'" + "OR Cedula LIKE '%" + txtBuscar.Text + "%'" + "OR ID LIKE '%" + txtBuscar.Text;
            dgvEmpleados.DataSource = data;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmEditarEmpleado frmEditarEmpleado = new FrmEditarEmpleado();
            frmEditarEmpleado.ShowDialog(this);

        }
    }
}
