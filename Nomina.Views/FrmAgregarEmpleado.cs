﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using Nomina.Business.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmAgregarEmpleado : Form
    {
        public DataSet DSNomina { get; set; }
        private DataRow drEmpleado;
        public int index;
        public int indexactual;
        DaoEmpleadoImplements dempleado = new DaoEmpleadoImplements();

        public FrmAgregarEmpleado()
        {
            InitializeComponent();
        }

        public void SetEmpleado(Empleado emp, int index)
        {
            maskCedula.Text = emp.Cedula;
            txtNombre.Text = emp.Nombres;
            txtApellido.Text = emp.Apellidos;
            txtDireccion.Text = emp.Direccion;
            maskTelefono.Text = emp.Telefono;
            maskSalario.Text = emp.Salario.ToString();
            datepick.Value = emp.FechaContratacion;
            this.index = index;
            this.indexactual = emp.Id;
        }

        //Metodo para guardar a un nuevo empleado
        private void btnAdd_Click(object sender, EventArgs e)//Este es el boton "Agregar"
        {

            drEmpleado = DSNomina.Tables["Empleado"].NewRow();

            if (txtNombre.Text == "" || txtApellido.Text == "" || maskCedula.Text == "" || maskTelefono.Text == "" || txtDireccion.Text == "" || maskSalario.Text == "")
            {
                MessageBox.Show("Error, No pueden haber campos vacios ,favor rellenarlos todos", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Empleado empleado = new Empleado()
            {
                Id = 0,
                Nombres = txtNombre.Text,
                Apellidos = txtApellido.Text,
                Cedula = maskCedula.Text,
                Telefono = maskTelefono.Text,
                Direccion = txtDireccion.Text,
                Salario = Convert.ToDecimal(maskSalario.Text),
                FechaContratacion = datepick.Value
            };

            drEmpleado["Id"] = empleado.Id;
            drEmpleado["Cedula"] = empleado.Cedula;
            drEmpleado["Nombres"] = empleado.Nombres;
            drEmpleado["Apellidos"] = empleado.Apellidos;
            drEmpleado["Direccion"] = empleado.Direccion;
            drEmpleado["Telefono"] = empleado.Telefono;
            drEmpleado["FechaContratacion"] = empleado.FechaContratacion;
            drEmpleado["Salario"] = empleado.Salario;

            //evitar que ingresen dos cedulas iguales para que al momento de buscar un empleado no haya problema
            if (dempleado.findByCedula(empleado.Cedula) != null)
            {
                MessageBox.Show("Error, La cedula ya existe, revise e ingrese una valida", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //Añadir a la tabla de la interfaz
            dempleado.Create(empleado);
            DSNomina.Tables["Empleado"].Rows.Add(drEmpleado);
            Dispose();
        }

        private void btnClean_Click(object sender, EventArgs e)// Este es el boton limpiar, por si anotaron demasiados datos incorrectos y no quieren borrar tantas veces
        {
            maskCedula.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtDireccion.Text = "";
            maskTelefono.Text = "";
            maskSalario.Text = "";

        }

        private void txtNombre_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyLetters(e);
        }

        private void maskCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyNumbers(e);
        }

        private void txtApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyLetters(e);
        }

        private void txtDireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyLetters(e);
        }

        private void maskTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyNumbers(e);
        }

        private void maskSalario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyNumbers(e);
        }
    }

}





