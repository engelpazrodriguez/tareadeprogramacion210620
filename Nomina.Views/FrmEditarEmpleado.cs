﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using Nomina.Business.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmEditarEmpleado : Form
    {
        private DataRow drEmpleado;
        private int index;
        private DaoEmpleadoImplements dempleado;
        private DataSet DSNomina;
        public FrmEditarEmpleado()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            drEmpleado = DSNomina.Tables["Empleado"].NewRow();

            if (txtNombre.Text == "" || txtApellido.Text == "" || maskCedula.Text == "" || maskTelefono.Text == "" || txtDireccion.Text == "" || maskSalario.Text == "")
            {
                MessageBox.Show("Error, No pueden haber campos vacios ,favor rellenarlos todos", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Empleado empleado = new Empleado()
            {
                Id = index + 1,
                Nombres = txtNombre.Text,
                Apellidos = txtApellido.Text,
                Cedula = maskCedula.Text,
                Telefono = maskTelefono.Text,
                Direccion = txtDireccion.Text,
                Salario = Convert.ToDecimal(maskSalario.Text),
                FechaContratacion = datepick.Value
            };

            drEmpleado["Id"] = index + 1;
            drEmpleado["Cedula"] = empleado.Cedula;
            drEmpleado["Nombres"] = empleado.Nombres;
            drEmpleado["Apellidos"] = empleado.Apellidos;
            drEmpleado["Direccion"] = empleado.Direccion;
            drEmpleado["Telefono"] = empleado.Telefono;
            drEmpleado["Salario"] = empleado.Salario;
            drEmpleado["FechaContratacion"] = empleado.FechaContratacion;

            //evitar que ingresen dos cedulas iguales para que al momento de buscar un empleado no haya problema
            if (dempleado.findByCedula(empleado.Cedula) != null)
            {
                MessageBox.Show("Error, La cedula ya existe, revise e ingrese una valida", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //Reemplaza los datos
            DSNomina.Tables["Empleado"].Rows.RemoveAt(index);
            DSNomina.Tables["Empleado"].Rows.InsertAt(drEmpleado, index);
            dempleado.Update(empleado);
            Dispose();
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyLetters(e);
        }

        private void maskCedula_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyNumbers(e);
        }

        private void txtApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyLetters(e);
        }

        private void txtDireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyLetters(e);
        }

        private void maskTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyNumbers(e);
        }

        private void maskSalario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validator.ValidateOnlyNumbers(e);
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            maskCedula.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtDireccion.Text = "";
            maskTelefono.Text = "";
            maskSalario.Text = "";
        }
    }
}

