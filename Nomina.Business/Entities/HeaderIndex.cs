﻿using System.Collections.Generic;
namespace Nomina.Business.Entities
{
    public class HeaderIndex
    {
        public int N { get; set; }
        public int K { get; set; }
        public string NameHeaderIndex { get; set; }
        public List<Index> Indices { get; set; }
    }
}

